import * as React from "react";
import type { NextPage } from "next";
import { ScoreCalculator } from "features/score";

const Home: NextPage = () => {
  return <ScoreCalculator />;
};

export default Home;
